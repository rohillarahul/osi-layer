# OSI LAYER

``
-> After reading this article you will be able to visualize how the two computer communicate over the network
``

## In this section we will be answering the following question
###
1. Why are we reading this.
1. Where it is located.
1. What is **OSI** Model and **Layer**


## Why we are reading this-
- Today we all know that without **Internet** we can't live.
- **Internet** means different computer devices are connected(**LAN or WAN**).
- **Connected** means they can talk to each other if any device need some resource(like any media file) which it don't have .
- To complete a successfully talk between each other computer(resources) both computer must talk in **same language, same grammar**.
- Or we can say same rule (same as a normal human being, Two people can talk to each other if they know each other language).
- Then the **OSI** model comes which is a **Collection Of Protocol** or **Set Of Rules**.
- OSI Model is implemented by **NIC**(Network Interface Card),**OS**,**Browser**(If the computer are communicating via browser). 
- To get the backside of computer system's communication process we are reading this article. 
- So that we can figure out how two computer are communicating remotely or locally.


## Where OSI Model located
## 
- It is mixture of **OS,NIC,Network Application,Router**

## What is OSI Model and its layer
### OSI model is a set of protocols or rules which is satisfied by network devices to communicate to each other.
### These protocols are known as layers which are following.
##
1. Application Layer
1. Presentation Layer
1. Session Layer
1. Transport Layer
1. Network Layer
1. Data Link Layer
1. Physical Layer

### 1.Application Layer -
Application layer is implemented by network application(like browser,skype,whatsapp etc).This is responsible for sending data at application layer which the end user can see.It is used for sending file from one application to other application().
This Layer is compromises of different protocols as following.
## 

1. HTTP- Transfering text file, image file etc.
1. FTP - File Transfer For locally.
1. TelNet -virtual terminal
1. SMTP - Mail Transfering

The data which is generated in application layer is send to Presentation Layer

### 2.Presentation Layer
Presentation layer take the data from application layer and convert it into binary data.
further that binary data is compressed to send effectively with no performance problem.

Text/string/Character data   -----(Presentation Layer)-->   binary data(compressed)


### 3.Session Layer

### 4.Transport Layer

### 5.Network Layer

### 6.Data Link Layer



























